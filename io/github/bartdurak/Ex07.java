package io.github.bartdurak;
// ! UWAZAJ . ćwiczenie dotyczy dekrementacji. Będę z tego pytał

public class Ex07 {
        /**
         * 7. Wczytaj liczbę naturalną. Rozpoczynając od wczytanej liczby wypisz, w kolejności malejącej,
         * wszystkie liczby do zera.
         * <p>
         * Podaj liczbę: 13
         * <p>
         *     kolejne oceny z przedmiotów matematyka, polski, angiellski, suma dwóch ocen z histrii
         jak niżej.
         *
         Nie wprowadzaj tych liczb, to jest przykład
         jak je wprowadzić , ty masz posługiwać się swoimi ocenami
         * 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
         */

        public static void main(String[] args) {

            int numer = 14;

            while (numer >= 0) {
                System.out.printf("%d%s", numer, numer > 0 ? ", " : "");

                numer--;
            }

        /*
        Rozwiązanie z użyciem pętli for

         for (; numer >= 0; numer--) {
         System.out.printf("%d%s", numer, numer > 0 ? ", " : "");
         }

        */
        }
    }

