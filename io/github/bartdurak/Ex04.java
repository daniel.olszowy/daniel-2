package io.github.bartdurak;

import java.util.Scanner;

public class Ex04 {

        public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);

            System.out.print("Podaj liczbę: ");
            int dzielnik = scanner.nextInt();

            boolean modul = dzielnik % dzielnik ;
            String resultat = modul == 0 ? "" : "nie";

            System.out.printf("Liczba %d jest %s", dzielnik, modul ? "parzysta" : "nieparzysta");

        }
}


