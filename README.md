Instrukcja instlowania token w InteliIDe


#### 1.1

Wypisz do konsoli tekst: `Witaj świecie!`

Do wypisania tekstu użyj metody: `print` lub `println` strumienia wyjściowego `System.out`


#### 1.3

Poproś użytkownika o wpisanie tekstu, następnie wypisz go w kolejnej linii:

```
Wpisz tekst: Witaj!
Wpisany tekst to: Witaj!
```

Do zaczytania tekstu użyj metody `next` klasy `Scanner`. Do konstruktora klasy `Scanner` przekaż domyślny strumień
wejściowy: `System.in`

#### 1.4

Wczytaj liczbę, następnie wypisz informację czy podana liczba jest parzysta czy nieparzysta.

```
 Uprzejmę proszę żeby każdy wymyślił sobie własne liczby np data urodzenia dzień razy 10 podzielony przez miesiąć urodzenia. 

Podaj liczbę: 88
Liczba 88 jest parzysta

Podaj liczbę: 13
Liczba 13 jest nieparzysta
```

Do wczytania liczby użyj metody `nextInt` klasy `Scanner`

#### 1.5
 Uprzejmę proszę żeby każdy wymyślił sobie własne liczby np data urodzenia dzień razy 10 podzielony przez miesiąć urodzenia. 
 

Wczytaj dwie liczby: dzielną i dzielnik, wypiszę informację czy można wykonać dzielenie całkowite.

```
Podaj dzielną: 88
Podaj dzielnik: 8

88 dzieli się przez 8
```

```
Podaj dzielną: 88
Podaj dzielnik: 9

88 nie dzieli się przez 9
```

#### 1.7

Wczytaj liczbę naturalną. Rozpoczynając od wczytanej liczby wypisz, w kolejności malejącej, wszystkie liczby do zera.

Podaj liczbę: 13
Nie wprowadzaj tych liczb niżej, to jest tylko przykład jak zapisać liczby przykład jak je wprowadzić , ty masz posługiwać się liczbami ze swoich ocen


13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0

#### 1.8

! Opis będzie w pliku. 

Znajdź najmniejszą i największą spośród wprowadzonych liczb rzeczywistych.

```
Podaj ilość liczb: 6

Podaj liczbę nr 1: 33.2
Podaj liczbę nr 2: 11
Podaj liczbę nr 3: 89.1
Podaj liczbę nr 4: 3.567
Podaj liczbę nr 5: 66.9
Podaj liczbę nr 6: 47

Najmniejsza z podanych liczb to: 3.567
Największa z podanych liczb to: 89.1
```

#### 1.9

Opis w pliku 

Napisz metodę wyznaczającą średnią arytmetyczną w tablicy liczb rzeczywistych.

